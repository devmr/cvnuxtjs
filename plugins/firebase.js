import firebase from 'firebase/app'
import 'firebase/firestore'

if (!firebase.apps.length) {
  const config = {
    apiKey: 'AIzaSyAdhqwnV95ChGxx5DLTtV1AOhYd0ajZGZI',
    authDomain: 'miaryrabs.firebaseapp.com',
    databaseURL: 'https://miaryrabs.firebaseio.com',
    projectId: 'miaryrabs',
    storageBucket: 'miaryrabs.appspot.com',
    messagingSenderId: '493485987620'
  }
  firebase.initializeApp(config)
  // firebase.firestore().settings({ timestampsInSnapshots: true })
}
const fireDb = firebase.firestore()
export { fireDb }
