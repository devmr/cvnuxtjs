module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Miary RABEHASY - Lead Développeur Passionné - powered by nuxtjs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:400,700' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {

    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }
  },

  modules: [
    'bootstrap-vue/nuxt',
    ['nuxt-fontawesome', {
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
        {
          set:'@fortawesome/free-brands-svg-icons',
          icons: ['fab']
        }
      ]
    }
    ],
    '@nuxtjs/axios',
    '@nuxtjs/recaptcha',
    ['@nuxtjs/google-analytics', {
      id: 'UA-89654867-3'
    }],
    ['@nuxtjs/markdownit', {
      injected: true
    }],
    [
      '@bazzite/nuxt-netlify',
      {
        mergeSecurityHeaders: true,
        netlify: {
          redirects: [
            {
              from: '/fahaizana',
              to: '/skill'
            }
          ]
        }
      }
    ]
  ],

  axios: {
    // proxyHeaders: false
  },

  recaptcha: {
    siteKey: '6LfNdaIUAAAAAGwMSKEmHMd4sRht-qemrqUOLWvr',    // Site key for requests
    version: 2,    // Version
  },

  plugins: [
    { src: '~/plugins/vue_chart.js', ssr: false },
    '~/plugins/firebase.js',
    '~/plugins/vue-moment.js',
    '~/plugins/vue-progressbar.js',
    '~/plugins/vuelidate.js',
    { src: '~/plugins/vue-cookie-law', ssr: false }
]
}

