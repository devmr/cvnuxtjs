# Miary R. resume. (powered by nuxtjs / VueJs / expressjs)

Hi ! this is my resume.

Deployed on :
- **netlify** (https://mrabehasy-nxt.netlify.com/)

## Tools

- [Bootstrap](https://getboostrap.com/) version 4.3
- [Vue-cli](https://vuejs.org/) version 3.5
- [Nuxtjs](https://nuxtjs.org/) version 2.6
- [Fontawesome](https://fontawesome.com/) version 5.0
- [Hamburgers](https://jonsuh.com/hamburgers/) version 1.1 
- [Apexcharts](https://apexcharts.com/) version 3.6 
- [Firebase](https://console.firebase.google.com/) version 3.6 
- [Progressbar.js](https://kimmobrunfeldt.github.io/progressbar.js/) version 1.0 

##
> Vue Components:
- [Bootstrap-vue](https://bootstrap-vue.js.org/) - v2.0.0-rc.18 
- nuxt-fontawesome - v0.4
- vue-apexcharts - v1.3 
- vue-moment - v4.0 
- vue-progress - v0.2 
- vuelidate - v0.7
- nuxtjs/axios - v5.4 
- @nuxtjs/recaptcha - v0.4 
- @nuxtjs/google-analytics - v2.2
- @nuxtjs/markdownit - v1.2
- @bazzite/nuxt-netlify - v0.1
- @nuxtjs/google-adsense - v1.1
- vue-cookie-law - v1.5

##
> Node package:
- node-sass

> Backend
- Firebase
- [expressjs node](https://mrabehasy-nodejs.herokuapp.com/) hosted on heroku ;)

 

